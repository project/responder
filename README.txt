Responder allows you to easily create responsive, mobile-first themes
in Drupal. It applies CSS3 @media queries to core and module CSS <link>
tags. This allows you to create mobile-aware layouts without having
to override countless module stylesheets.

----------------------------------------------------------------------

You have checkout out the master branch. Nothing to see here.
Please check out a branch starting with a major version of Drupal:

----------------------------------------------------------------------

git checkout 6.x-1.x

- or better yet -

git checkout 7.x-1.x
